public class Role{
    public Role(
        String name,
        String[] permissions
    ){
        this.name = name;
        this.permissions = permissions;
    }


    String name;
    public String getName(){
        return name;
    }
    
    public void setName( String name ){
        this.name = name;
    }


    String[] permissions;
    public String[] getPermissions(){
        return permissions;
    }
    
    public void setPermissions( String[] permissions ){
        this.permissions = permissions;
    }
}