public class User{
    public User(
        String login,
        String pass,
        String phone,
        String[] addresses,
        Role[] roles
    ){
        this.login = login;
        this.pass = pass;
        this.phone = phone;
        this.addresses = addresses;
        this.roles = roles;
    }


    String login;
    public String getLogin(){
        return login;
    }
    
    public void setLogin( String login ){
        this.login = login;
    }


    String pass;
    public String getPass(){
        return pass;
    }
    
    public void setPass( String pass ){
        this.pass = pass;
    }


    String phone;
    public String getPhone(){
        return phone;
    }
    
    public void setPhone( String phone ){
        this.phone = phone;
    }


    String[] addresses;
    public String[] getAddresses(){
        return addresses;
    }
    
    public void setAddresses( String[] addresses ){
        this.addresses = addresses;
    }


    Role[] roles;
    public Role[] getRoles(){
        return roles;
    }
    
    public void setRoles( Role[] roles ){
        this.roles = roles;
    }
}