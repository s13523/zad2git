
public class TestUser{
    public static void main( String[] arguments ){

        User testValue = new User(
            "some string value", 
            "some string value", 
            "some string value", 
            null, 
            null
        );
        if( !testValue.getLogin().equals("some string value") ){
            System.out.println( "test doesn't pass for Login" );
        }

        if( !testValue.getPass().equals("some string value") ){
            System.out.println( "test doesn't pass for Pass" );
        }

        if( !testValue.getPhone().equals("some string value") ){
            System.out.println( "test doesn't pass for Phone" );
        }

    }
}